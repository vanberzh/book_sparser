import scrapy
from ..items import BooksParserItem


class DkSpbSpider(scrapy.Spider):
    name = 'dk_spb'
    allowed_domains = ['www.dk-spb.ru']
    start_urls = ['http://www.dk-spb.ru/']
    page_count = 5

    def start_requests(self):
        for page in range(1, 1 + self.page_count):
            url = f"https://dk-spb.ru/books/xudozhestvennaya-literatura/?page={page}"
            yield scrapy.Request(url, callback= self.parse_pages)

    def parse_pages(self, response, **kwargs):
        for href in response.xpath("//div[@class = 'pcol__list']//a/@href").getall():
            url = response.urljoin(href)
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        item = BooksParserItem
        item['name'] = response.xpath("//div[@class = 'pdet__entry']/h1/text()").get()
        item['author'] = response.xpath("//div[@class = 'pdet__char']//div[@class = 'pdet__char-val']/a/text()").getall()[-1]
        item['price'] = response.xpath("//div[@class = 'pdet__price-new']/span/text()").get()
        yield item
